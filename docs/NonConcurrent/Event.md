# Event Non-Concurrent

## Tutorial

The tutorial you provided is comprehensive but contains a few inaccuracies. I've made corrections to ensure consistency with the overall narrative:
Tutorial

 - For demonstration purposes, create three files: event_democallback.h, event_democallback_impl.c, and main.c. Also, clone the repository via: git clone https://codeberg.org/TylerLinuxDev/STL-For-C.
 - In event_democallback.h, you should include a header guard, define the EVENT_ARG_TYPE variable, and include the header code for the event. It should look like this:

```c
#ifndef EVENT_DEMOCALLBACK_H
#define EVENT_DEMOCALLBACK_H

#include <stdint.h> // For int32_t definition

typedef struct {
    int32_t A;
    int32_t B;
} DemoEventArg;

#define EVENT_ARG_TYPE DemoEventArg
#include "STL_For_C/include/event.h"

#endif
```

In event_democallback_impl.c, define the EVENT_ARG_TYPE variable and include the event implementation:

```c
#include "event_democallback.h"
#define EVENT_ARG_TYPE DemoEventArg
#include "STL_For_C/src/event_impl.c"
```

Finally, in main.c, you can use various functions provided by the event type as follows:

```c
#include "event_democallback.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdatomic.h>

static _Atomic int32_t val;

void Callback(DemoEventArg arg)
{
    val += arg.A - arg.B;
}

int main()
{
    event_DemoEventArg* event = event_new_DemoEventArg();
    event_add_DemoEventArg(event, Callback);
    event_add_DemoEventArg(event, Callback);
    event_invoke_DemoEventArg(event, (DemoEventArg){.A = 4, .B = 2});
    printf("%i == 4 = %s\n", val, (val == 4 ? "True" : "False"));
    event_destroy_DemoEventArg(event);

    return 0;
}
```

Compile your program by running: gcc -o MyProgram event_democallback_impl.c main.c -I.

Please note that you should replace CC with gcc or any other compiler you're using. Furthermore, the include flag -I. is added to the command to tell the compiler to look for header files in the current directory. This assumes that the STL_For_C directory is in the same directory where you are running this command.

## Files ##

* [STL_For_C/include/event.h](https://codeberg.org/TylerLinuxDev/STL-For-C/src/branch/main/include/event.h)
* [STL_For_C/src/event_impl.c](https://codeberg.org/TylerLinuxDev/STL-For-C/src/branch/main/src/event_impl.c)

## Variables ##

<table>
<tr><th><font size="5">Variable Name</font></th><th><font size="5">Description</font></th></tr>
<tr><td><b>EVENT_ARG_TYPE</b></td><td>Defines the type of event argument to be passed into the event handler. <br/> <b>Example:</b>

```c
#define EVENT_ARG_TYPE int32_t
```

<b>Note:</b> The type name should not contain any spaces or asterisks as this will result in a preprocessing error. Use typedef instead. <br /><br /><b>DO:</b> <br/>

```c
typedef int32_t* ptrToInt32_t;
#define EVENT_ARG_TYPE ptrToInt32_t
```

<b>DON'T:</b> <br/>

```c
#define EVENT_ARG_TYPE int32_t*
```

</td></tr>
<tr><td><b>INDEX_TYPE</b></td><td>Defines an integer or floating point type for the List to use for tracking indices and sizes. By default, this is uint64_t unless specified otherwise. Structures should not be used for this type, it should either be a floating point or integer type that supports arithmetic operations. <br/><br /><b>DO:</b> <br/>

```c
#define INDEX_TYPE int32_t
```

<b>DON'T:</b>

```c
typedef struct { float a; } MyStruct;
#define INDEX_TYPE MyStruct
```

<b>WARNING:</b> Integer underflow or overflow is still a risk if you choose to use smaller sized data types. If floating point is chosen for index it may result in data corruption. Therefore, evaluate whether INDEX_TYPE should be changed with this in mind. If it is changed, ensure you keep INDEX_TYPE consistent across all data containers, since other data containers may use the underlying implementation. Both unsigned and signed integers can be used.</td></tr>
<tr><td><b>MINIMUM_LIST_SIZE</b></td><td>Defines the minimum size as an integer or floating point value. By default, this is set to a 32 items capacity whenever an event is allocated without a specified capacity, or when clearing the event, which will result in reallocating the event back to a 32 items capacity. It's recommended to leave this unchanged. <br/><br /><b>DO:</b>

```c
#define MINIMUM_LIST_SIZE 32
```

<b>DON'T:</b>

```c
#define MINIMUM_LIST_SIZE -1
```

<b>or</b>

```c
#define MINIMUM_LIST_SIZE int
```

<b>WARNING:</b> If specified to 0 or below, this will result in arithmetic and memory allocation errors.</td></tr>
</table>

## Functions

Replace X for EVENT_ARG_TYPE for the followings:

| Functions | Description |
| :-- | :-- |
| <span style="color:red;">event_X\*</span> event_X_new() | **Returns:** <span style="color:red;">event_X*</span> - Newly allocated event object <br/> **Description:** Allocates a new event_X object with a default capacity of 32 or a custom capacity defined by the MINIMUM_LIST_SIZE variable. |
| <span style="color:red">bool</span> event_destroy_X(<span style="color:green;">event_X* this</span>) | **Parameter 1:** <span style="color:green;">event_X* this</span> - Reference to the event object. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true if event object is NULL; false otherwise. <br/> **Description:** Disposes of any memory allocated by the event_X object. Note that this does not include memory managed by elements within the event object, which must be handled separately. |
| <span style="color:red">bool</span> event_add_X(<span style="color:green">event_X* this</span>, <span style="color:purple">callback_X item</span>) | **Parameter 1:** <span style="color:green">event_X* this</span> - Reference to the event object. <br/> **Parameter 2:** <span style="color:purple">callback_X item</span> - Callback function pointer accepting X arguments. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on error (invalid event or failed memory reallocation); false on success. <br/> **Description:** Adds a callback function pointer to the event object. If the added callback exceeds the event's capacity, the capacity is automatically doubled. |
| <span style="color:red">bool</span> event_remove_X(<span style="color:green">event_X* this</span>, <span style="color:purple">callback_X callback</span>) | **Parameter 1:** <span style="color:green">event_X* this</span> - Reference to the event object. <br/> **Parameter 2:** <span style="color:purple">callback_X callback</span> - Callback function pointer to remove from the event object. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on error (invalid event, invalid function pointer, or failed memory reallocation); false on success. <br/> **Description:** Removes a callback function pointer from the event object. If the removal causes the event's capacity to exceed four times the number of stored items, the capacity is halved. |
| <span style="color:red">bool</span> event_X_clear(<span style="color:green">event_X* this</span>) | **Parameter 1:** <span style="color:green">event_X* this</span> - Reference to the event object. <br/> **Returns:** Returns true if this parameter is invalid; false on success. <br/> **Description:** Clears all callback function pointers from the event object and reallocates memory to hold 32 items or a custom number defined by the MINIMUM_LIST_SIZE variable. |
| <span style="color:red">bool</span> event_count_X(<span style="color:green">event_X* this</span>, <span style="color:purple">INDEX_TYPE* outCount</span>) | **Parameter 1:** <span style="color:green">event_X* this</span> - Reference to the event object. <br/> **Parameter 2:** <span style="color:purple">INDEX_TYPE* outCount</span> - Pointer to an INDEX_TYPE variable to store the count of items in the event object. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on error (invalid event); false on success. <br/> **Description:** Retrieves the count of callback function pointers stored in the event object. |
| <span style="color:red">bool</span> event_invoke_X(<span style="color:green">event_X* this</span>, <span style="color:purple">X arg</span>) | **Parameter 1:** <span style="color:green">event_X* this</span> - Reference to the event object. <br/> **Parameter 2:** <span style="color:purple">X arg</span> - Argument to pass to the stored callback functions. <br/> **Returns:** Returns true if this parameter is invalid; false on success. <br/> **Description:** Invokes each callback function stored in the event object, passing the arg argument to each one. |

### Implementation Specific Notes

:warning: **Memory Allocation:** Be aware that whenever an element is added to the Event and this action exceeds its current capacity, the Event will automatically reallocate memory, doubling its current capacity size. Conversely, if the number of elements is reduced to less than a quarter of the total capacity, the Event may also reallocate memory to halve its capacity size. This approach helps manage memory usage effectively, but it's important to consider this behavior during implementation as it can influence performance.

**Thread Safety:** ❌ - This implementation is not designed for thread safety. If you require a thread-safe implementation, please refer to concurrent_event which has been designed with thread safety in mind. Be sure to understand the differences and implications of these two different implementations to ensure they meet your project's needs.
