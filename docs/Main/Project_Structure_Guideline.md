## Project Structure Guideline

This guideline discusses two methods for using data container types in your project:

 - Replace Text Method
 - Header Inclusion Method

This document elaborates on the Header Inclusion Method.

## Header Inclusion Method

The Header Inclusion Method involves creating a dedicated header file for each data container type. The header file includes all relevant data container headers and defines variables for each container.

This approach provides a reliable way to manage data container types and their dependencies.
Implementation Steps:

 - Create a header file

 - You should create a header file to include relevant data container header files with defined variables for each container.

 - Example: MyLists.h

```c
#ifndef MY_LIST_H
#define MY_LIST_H

#include <stdint.h>

#define LIST_TYPE int64_t
#include "STL_For_C/include/list.h"

#define LIST_TYPE int32_t
#include "STL_For_C/include/list.h"

// Additional data container headers as needed...

#endif
```

This example demonstrates how to define multiple list types. Notice that each LIST_TYPE definition is immediately followed by the inclusion of list.h.

Note: The data container header files don't have header guards to prevent duplicate listing, so you must enforce this by creating separate header files.

Create an implementation file

After creating the header file, you should define an implementation file that includes the same list of data container header files.

Example: MyList.c

```c
#define LIST_TYPE int64_t
#include "STL_For_C/src/list.h"

#define LIST_TYPE int32_t
#include "STL_For_C/src/list.h"

// Additional data container source files as needed...
```

Ensure that MyList.c is linked with the rest of your program.

Use the list in your program

Now you can use the newly created list types in your project.

Example: main.c

```c
    #include "MyList.h"

    int main()
    {
        list_int32_t* list = list_int32_t_new();
        // Your code here...
        list_int32_t_destroy(list);
        return 0;
    }
```
    This code snippet demonstrates how to create, use, and destroy an int32_t list.

## Limitations

The Header Inclusion Method has a limitation regarding function documentation. Due to the preprocessing step, auto-generated documentation for each function may be absent. For this reason, the Replace Text Method might be more suitable if the availability of function documentation is critical. However, the Header Inclusion Method is easier to use and manage, making it a preferred choice for most cases.
