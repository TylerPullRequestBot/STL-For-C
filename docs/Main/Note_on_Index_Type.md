## Index Type Consistency in STL for C

This document provides guidelines for modifying the INDEX_TYPE in STL for C, emphasizing the importance of consistency across all types to prevent stack and memory corruption.

### Important Note

Do not change INDEX_TYPE unless necessary. If a change is required, ensure that it is consistently defined across all types in STL for C.

## Rationale

In the STL for C project, INDEX_TYPE is a critical component in C header and source files. Its primary function is to denote the index type used in data structures.

Inconsistent definition of INDEX_TYPE across different generic types can lead to stack or memory corruption corruption. This issue occurs when one generic type, expecting a particular index type, interacts with another generic type that has a different index type defined.

For instance, if INDEX_TYPE is defined as int in one generic type and as long in another, a function that expects an int index might mistakenly receive a long index, potentially leading to stack corruption.
Recommendations

To prevent potential stack and memory corruption, it is recommended to:

 - Avoid unnecessary changes to INDEX_TYPE. The default INDEX_TYPE has been carefully chosen to balance performance and compatibility across different systems.

 - Maintain consistency of INDEX_TYPE. If changing INDEX_TYPE is unavoidable, ensure that it is consistently defined the same across all types in STL for C.

By following these guidelines, you can help prevent potential stack and memory corruption and ensure the stable operation of your STL for C project.
