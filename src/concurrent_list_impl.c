/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
    List Type reimplemented in C
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdatomic.h>
#ifndef CONCURRENT_LIST_TYPE
    #error There must be a provided type for CONCURRENT_LIST_TYPE and it must be defined prior to using this header!
    #define CONCURRENT_LIST_TYPE int32_t
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#include <stdlib.h>
#include <string.h>
    
#ifndef MINIMUM_CONCURRENT_LIST_SIZE
#define MINIMUM_CONCURRENT_LIST_SIZE 32
#endif

#define MAKE_CONCURRENT_LIST_NAME(x) concurrent_list_ ## x
#define CONCURRENT_LIST_NAME(x) MAKE_CONCURRENT_LIST_NAME(x)
#define CONCURRENT_LIST CONCURRENT_LIST_NAME(CONCURRENT_LIST_TYPE)

typedef struct {
    INDEX_TYPE capacity;
    INDEX_TYPE size;
    CONCURRENT_LIST_TYPE *items;
    atomic_int_fast32_t list_mutex;

} CONCURRENT_LIST;

#define MAKE_CONCURRENT_LIST_NEW_NAME(x) x ## _new()
#define GEN_CONCURRENT_LIST_NEW_NAME(x) MAKE_CONCURRENT_LIST_NEW_NAME(x)
// concurrent_list_x* concurrent_list_x_new()
CONCURRENT_LIST* GEN_CONCURRENT_LIST_NEW_NAME(CONCURRENT_LIST)
{
    CONCURRENT_LIST* newData = (CONCURRENT_LIST*) malloc(sizeof(CONCURRENT_LIST));
    if (newData == NULL)
        return NULL;
    newData->items = calloc(sizeof(CONCURRENT_LIST_TYPE), MINIMUM_CONCURRENT_LIST_SIZE);
    if (newData->items == NULL)
    {
        free(newData);
        return NULL;
    }
    newData->capacity = MINIMUM_CONCURRENT_LIST_SIZE;
    newData->size = 0;
    return newData;
}

#define MAKE_CONCURRENT_LIST_NEW_WITHCAPACITY_NAME(x) x ## _new_withcapacity(INDEX_TYPE initialCapacity)
#define GEN_CONCURRENT_LIST_NEW_WITHCAPACITY_NAME(x) MAKE_CONCURRENT_LIST_NEW_WITHCAPACITY_NAME(x)
// concurrent_list_x* concurrent_list_x_new_withcapacity(INDEX_TYPE initialCapacity)
CONCURRENT_LIST* GEN_CONCURRENT_LIST_NEW_WITHCAPACITY_NAME(CONCURRENT_LIST)
{
    CONCURRENT_LIST* newData = (CONCURRENT_LIST*) malloc(sizeof(CONCURRENT_LIST));
    if (newData == NULL && initialCapacity <= 0)
        return NULL;
    newData->items = calloc(sizeof(CONCURRENT_LIST_TYPE), initialCapacity);
    if (newData->items == NULL)
    {
        free(newData);
        return NULL;
    }
    newData->capacity = initialCapacity;
    newData->size = 0;
    return newData;
}

#define MAKE_CONCURRENT_LIST_LOCK_NAME(x) x ## _lock(x* this)
#define GEN_CONCURRENT_LIST_LOCK_NAME(x) MAKE_CONCURRENT_LIST_LOCK_NAME(x)
// bool concurrent_list_x_lock(concurrent_list_x* this)
bool GEN_CONCURRENT_LIST_LOCK_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    atomic_int_fast32_t expected = 0;
    while (atomic_compare_exchange_strong(&this->list_mutex, &expected, 1))
    {

    }
    return false;
}

#define MAKE_CONCURRENT_LIST_DESTROY_NAME(x) x ## _destroy(x* this)
#define GEN_CONCURRENT_LIST_DESTROY_NAME(x) MAKE_CONCURRENT_LIST_DESTROY_NAME(x)
// bool concurrent_list_x_destroy(concurrent_list_x* this)
bool GEN_CONCURRENT_LIST_DESTROY_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    free(this->items);
    free(this);
    return false;
}

#define MAKE_CONCURRENT_LIST_INDEXOF_NAME(x) x ## _indexof(x* this, CONCURRENT_LIST_TYPE searchItem, INDEX_TYPE* outIndex)
#define GEN_CONCURRENT_LIST_INDEXOF_NAME(x) MAKE_CONCURRENT_LIST_INDEXOF_NAME(x)
#define MAKE_CONCURRENT_LIST_INDEXOF_NAME_USE(x) x ## _indexof
#define GEN_CONCURRENT_LIST_INDEXOF_NAME_USE(x) MAKE_CONCURRENT_LIST_INDEXOF_NAME_USE(x)
// bool concurrent_list_x_indexof(concurrent_list_x* this, INDEX_TYPE* outIndex)
bool GEN_CONCURRENT_LIST_INDEXOF_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    for (INDEX_TYPE i = 0; i < this->size; ++i)
    {
#ifndef CONCURRENT_LIST_TYPE_CMP_FUNCTION
        if (memcmp(&this->items[i], &searchItem, sizeof(CONCURRENT_LIST_TYPE)) == 0)
        {
            *outIndex = i;
            return false;
        }
#else
        if (CONCURRENT_LIST_TYPE_CMP_FUNCTION(this->items[i], searchItem) == true)
        {
            *outIndex = i;
            return false;
        }
#endif
    }
    return true;
}

#define MAKE_CONCURRENT_LIST_INDEXOF2_NAME(x) x ## _indexof2(x* this, CONCURRENT_LIST_TYPE searchItem, INDEX_TYPE start, INDEX_TYPE* outIndex)
#define GEN_CONCURRENT_LIST_INDEXOF2_NAME(x) MAKE_CONCURRENT_LIST_INDEXOF2_NAME(x)
// bool list_x_indexof2(list_x* this, INDEX_TYPE start, INDEX_TYPE* outIndex)
bool GEN_CONCURRENT_LIST_INDEXOF2_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    for (INDEX_TYPE i = start; i < this->size; ++i)
    {
#ifndef CONCURRENT_LIST_TYPE_CMP_FUNCTION
        if (memcmp(&this->items[i], &searchItem, sizeof(CONCURRENT_LIST_TYPE)) == 0)
        {
            *outIndex = i;
            return false;
        }
#else
        if (CONCURRENT_LIST_TYPE_CMP_FUNCTION(this->items[i], searchItem) == true)
        {
            *outIndex = i;
            return false;
        }
#endif
    }
    return true;
}

#define MAKE_CONCURRENT_LIST_INDEXOF3_NAME(x) x ## _indexof3(x* this, CONCURRENT_LIST_TYPE searchItem, INDEX_TYPE start, INDEX_TYPE length, INDEX_TYPE* outIndex)
#define GEN_CONCURRENT_LIST_INDEXOF3_NAME(x) MAKE_CONCURRENT_LIST_INDEXOF3_NAME(x)
// bool list_x_indexof3(list_x* this, INDEX_TYPE start, INDEX_TYPE length, INDEX_TYPE* outIndex)
bool GEN_CONCURRENT_LIST_INDEXOF3_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    INDEX_TYPE end = start + length;
    if (end > this->size)
        end = this->size;
    for (INDEX_TYPE i = start; i < end; ++i)
    {
#ifndef CONCURRENT_LIST_TYPE_CMP_FUNCTION
        if (memcmp(&this->items[i], &searchItem, sizeof(CONCURRENT_LIST_TYPE)) == 0)
        {
            *outIndex = i;
            return false;
        }
#else
        if (CONCURRENT_LIST_TYPE_CMP_FUNCTION(this->items[i], searchItem) == true)
        {
            *outIndex = i;
            return false;
        }
#endif
    }
    return true;
}

#define MAKE_CONCURRENT_LIST_GET_NAME(x) x ## _get(x* this, INDEX_TYPE index, CONCURRENT_LIST_TYPE* result)
#define GEN_CONCURRENT_LIST_GET_NAME(x) MAKE_CONCURRENT_LIST_GET_NAME(x)
// bool list_x_get(list_x* this, INDEX_TYPE index, CONCURRENT_LIST_TYPE* result)
bool GEN_CONCURRENT_LIST_GET_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    if (index >= 0 && index < this->size)
    {
        *result = this->items[index];
        return false;
    }
    return true;
}

#define MAKE_CONCURRENT_LIST_GET_FIRST_NAME(x) x ## _get_first(x* this, CONCURRENT_LIST_TYPE* result)
#define GEN_CONCURRENT_LIST_GET_FIRST_NAME(x) MAKE_CONCURRENT_LIST_GET_FIRST_NAME(x)
// bool list_x_get_first(list_x* this, CONCURRENT_LIST_TYPE* result)
bool GEN_CONCURRENT_LIST_GET_FIRST_NAME(CONCURRENT_LIST)
{
    if (this == NULL || this->size <= 0) return true;
    *result = this->items[0];
    return false;
}

#define MAKE_CONCURRENT_LIST_GET_LAST_NAME(x) x ## _get_last(x* this, CONCURRENT_LIST_TYPE* result)
#define GEN_CONCURRENT_LIST_GET_LAST_NAME(x) MAKE_CONCURRENT_LIST_GET_LAST_NAME(x)
// bool list_x_get_list(list_x* this, CONCURRENT_LIST_TYPE* result)
bool GEN_CONCURRENT_LIST_GET_LAST_NAME(CONCURRENT_LIST)
{
    if (this == NULL || this->size <= 0) return true;
    *result = this->items[this->size - 1];
    return false;
}

#define MAKE_CONCURRENT_LIST_GETRANGE_NAME(x) x ## _getrange(x* this, INDEX_TYPE index, INDEX_TYPE length, CONCURRENT_LIST_TYPE* result)
#define GEN_CONCURRENT_LIST_GETRANGE_NAME(x) MAKE_CONCURRENT_LIST_GETRANGE_NAME(x)
// bool list_x_getrange(list_x* this, INDEX_TYPE index, INDEX_TYPE length, CONCURRENT_LIST_TYPE* result)
bool GEN_CONCURRENT_LIST_GETRANGE_NAME(CONCURRENT_LIST)
{
    if (this == NULL || result == NULL) return true;
    if (index < 0 || index >= this->size || length < 0)
        return true;
    INDEX_TYPE remainder = this->size - index;
    if (length > remainder)
        return true;
    
    memcpy(result, &this->items[index], length * sizeof(CONCURRENT_LIST_TYPE));
    return false;
}
#define MAKE_CONCURRENT_LIST_REALLOCATE_NAME(x) x ## _reallocate(x* this, INDEX_TYPE newLength)
#define GEN_CONCURRENT_LIST_REALLOCATE_NAME(x) MAKE_CONCURRENT_LIST_REALLOCATE_NAME(x)
#define MAKE_CONCURRENT_LIST_REALLOCATE_NAME_USE(x) x ## _reallocate
#define GEN_CONCURRENT_LIST_REALLOCATE_NAME_USE(x) MAKE_CONCURRENT_LIST_REALLOCATE_NAME_USE(x)
// bool list_x_reallocate(list_x* this, INDEX_TYPE newLength)
bool GEN_CONCURRENT_LIST_REALLOCATE_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return false;
    if (newLength <= 0)
    {
        free(this->items);
        this->items = NULL;
        this->size = 0;
        this->capacity = 0;
        return false;
    }

    if (newLength < this->capacity)
    {
        CONCURRENT_LIST_TYPE* temp = (CONCURRENT_LIST_TYPE*) realloc(this->items, newLength * sizeof(CONCURRENT_LIST_TYPE));

        if (temp == NULL) { return true; }
        
        this->items = temp;
        this->capacity = newLength;

        if (this->size > newLength) { this->size = newLength; }
        return false;
    }
    
    INDEX_TYPE newLen = this->capacity * 2;
    
    // In case if you're wondering, this double the size until overflow happens
    while (newLen < newLength && newLen < newLen * 2) { newLen *= 2; }
    
    if (newLen < newLength) { return true; }
    
    CONCURRENT_LIST_TYPE* temp = (CONCURRENT_LIST_TYPE*) realloc(this->items, newLen * sizeof(CONCURRENT_LIST_TYPE));
    
    if (temp == NULL) { return true; }

    this->items = temp;
    this->capacity = newLen;
    return false;
}

#define MAKE_CONCURRENT_LIST_ADD_NAME(x) x ## _add(x* this, CONCURRENT_LIST_TYPE item)
#define GEN_CONCURRENT_LIST_ADD_NAME(x) MAKE_CONCURRENT_LIST_ADD_NAME(x)
// bool list_x_add(list_x* this, CONCURRENT_LIST_TYPE item)
bool GEN_CONCURRENT_LIST_ADD_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    if (this->size >= this->capacity)
    {
        bool check = GEN_CONCURRENT_LIST_REALLOCATE_NAME_USE(CONCURRENT_LIST)(this, this->size + 1);
        if (check) { return true; }
    }
    this->items[this->size++] = item;
    return false;
}

#define MAKE_CONCURRENT_LIST_ADDRANGE_NAME(x) x ## _addrange(x* this, CONCURRENT_LIST_TYPE* items, INDEX_TYPE items_length)
#define GEN_CONCURRENT_LIST_ADDRANGE_NAME(x) MAKE_CONCURRENT_LIST_ADDRANGE_NAME(x)
// bool list_x_addrange(list_x* this, CONCURRENT_LIST_TYPE* items, INDEX_TYPE items_length)
bool GEN_CONCURRENT_LIST_ADDRANGE_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    INDEX_TYPE newSize = this->size + items_length;
    if (newSize >= this->capacity)
    {
        bool check = GEN_CONCURRENT_LIST_REALLOCATE_NAME_USE(CONCURRENT_LIST)(this, newSize);
        if (check == true) return true;
    }
    memcpy(&this->items[this->size], items, items_length * sizeof(CONCURRENT_LIST_TYPE));
    this->size += items_length;
    return false;
}

#define MAKE_CONCURRENT_LIST_ADD_AT_NAME(x) x ## _add_at(x* this, CONCURRENT_LIST_TYPE item, INDEX_TYPE index)
#define GEN_CONCURRENT_LIST_ADD_AT_NAME(x) MAKE_CONCURRENT_LIST_ADD_AT_NAME(x)
// bool list_x_add(list_x* this, CONCURRENT_LIST_TYPE item, INDEX_TYPE index)
bool GEN_CONCURRENT_LIST_ADD_AT_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    if (this->size >= this->capacity)
    {
        bool check = GEN_CONCURRENT_LIST_REALLOCATE_NAME_USE(CONCURRENT_LIST)(this, this->size + 1);
        if (check == false) { return true; }
    }
    INDEX_TYPE remainder = this->size - index;
    memmove(&this->items[index + 1], &this->items[index], (remainder + 1) * sizeof(CONCURRENT_LIST_TYPE));
    this->items[index] = item;
    this->size++;
    return false;
}

#define MAKE_CONCURRENT_LIST_ADDRANGE_AT_NAME(x) x ## _addrange_at(x* this, INDEX_TYPE index, CONCURRENT_LIST_TYPE* items, INDEX_TYPE items_length)
#define GEN_CONCURRENT_LIST_ADDRANGE_AT_NAME(x) MAKE_CONCURRENT_LIST_ADDRANGE_AT_NAME(x)
// bool list_x_addrange_at(list_x* this, INDEX_TYPE index, CONCURRENT_LIST_TYPE* items, INDEX_TYPE items_length)
bool GEN_CONCURRENT_LIST_ADDRANGE_AT_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    if (items_length <= 0 || items == NULL || index < 0 || (this->size > 0 && index >= this->size))
        return true;
    INDEX_TYPE newLen = this->size + items_length;
    if (newLen >= this->capacity)
    {
        if (GEN_CONCURRENT_LIST_REALLOCATE_NAME_USE(CONCURRENT_LIST)(this, newLen))
            return true;
    }
    INDEX_TYPE remainder = this->size - index;
    if (remainder > 0)
        memmove(&this->items[index + items_length], &this->items[index], remainder * sizeof(CONCURRENT_LIST_TYPE));
    memcpy(&this->items[index], items, items_length * sizeof(CONCURRENT_LIST_TYPE));
    this->size = newLen;
    return false;
}

#define MAKE_CONCURRENT_LIST_REMOVE_NAME(x) x ## _remove(x* this, INDEX_TYPE index)
#define GEN_CONCURRENT_LIST_REMOVE_NAME(x) MAKE_CONCURRENT_LIST_REMOVE_NAME(x)
#define MAKE_CONCURRENT_LIST_REMOVE_NAME_USE(x) x ## _remove
#define GEN_CONCURRENT_LIST_REMOVE_NAME_USE(x) MAKE_CONCURRENT_LIST_REMOVE_NAME_USE(x)
// bool list_x_remove(list_x* this, INDEX_TYPE index)
bool GEN_CONCURRENT_LIST_REMOVE_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    INDEX_TYPE remainder = this->size - index;
    if (remainder > 0)
    {
        memmove(&this->items[index], &this->items[index + 1], remainder * sizeof(CONCURRENT_LIST_TYPE));
        if (this->size > 0)
            this->size -= 1;
    }
    if (this->size < this->capacity / 4)
    {
        return GEN_CONCURRENT_LIST_REALLOCATE_NAME_USE(CONCURRENT_LIST)(this, this->capacity / 2);
    }
    return false;
}


#define MAKE_CONCURRENT_LIST_REMOVERANGE_NAME(x) x ## _removerange(x* this, INDEX_TYPE index, INDEX_TYPE length)
#define GEN_CONCURRENT_LIST_REMOVERANGE_NAME(x) MAKE_CONCURRENT_LIST_REMOVERANGE_NAME(x)
// bool list_x_removerange(list_x* this, INDEX_TYPE index, INDEX_TYPE length)
bool GEN_CONCURRENT_LIST_REMOVERANGE_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    if (index >= this->size || index < 0)
        return true;
    INDEX_TYPE remainderSize = 0;
    if (this->size > index + length)
        remainderSize = this->size - index - length;
    if (remainderSize <= 0)
    {
        this->size = index;
        if (this->size < this->capacity / 4)
        {
            return GEN_CONCURRENT_LIST_REALLOCATE_NAME_USE(CONCURRENT_LIST)(this, this->capacity / 2);
        }
        return false;
    }

    if (length < 0)
        length = 0;
    memmove(&this->items[index], &this->items[index + length], remainderSize * sizeof(CONCURRENT_LIST_TYPE));
    this->size -= length;
    
    if (this->size < 0)
        this->size = 0;
    
    if (this->size < this->capacity / 4)
    {
        return GEN_CONCURRENT_LIST_REALLOCATE_NAME_USE(CONCURRENT_LIST)(this, this->capacity / 2);
    }
    return false;
}


#define MAKE_CONCURRENT_LIST_REMOVE_ITEM_NAME(x) x ## _remove_item(x* this, CONCURRENT_LIST_TYPE item)
#define GEN_CONCURRENT_LIST_REMOVE_ITEM_NAME(x) MAKE_CONCURRENT_LIST_REMOVE_ITEM_NAME(x)
// bool list_x_remove_item(list_x* this, INDEX_TYPE index)
bool GEN_CONCURRENT_LIST_REMOVE_ITEM_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    INDEX_TYPE index = 0;
    if (GEN_CONCURRENT_LIST_INDEXOF_NAME_USE(CONCURRENT_LIST)(this, item, &index))
        return true;
    return GEN_CONCURRENT_LIST_REMOVE_NAME_USE(CONCURRENT_LIST)(this, index);
}

#define MAKE_CONCURRENT_LIST_CLEAR_NAME(x) x ## _clear(x* this)
#define GEN_CONCURRENT_LIST_CLEAR_NAME(x) MAKE_CONCURRENT_LIST_CLEAR_NAME(x)
// bool list_x_clear(list_x* this)
bool GEN_CONCURRENT_LIST_CLEAR_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    if (this->items != NULL)
        free(this->items);
    this->items = calloc(sizeof(CONCURRENT_LIST_TYPE), MINIMUM_CONCURRENT_LIST_SIZE);
    this->capacity = MINIMUM_CONCURRENT_LIST_SIZE;
    this->size = 0;
    return false;
}

#define MAKE_CONCURRENT_LIST_COUNT_NAME(x) x ## _count(x* this, INDEX_TYPE* outCount)
#define GEN_CONCURRENT_LIST_COUNT_NAME(x) MAKE_CONCURRENT_LIST_COUNT_NAME(x)
// bool list_x_count(list_x* this, INDEX_TYPE* outCount)
bool GEN_CONCURRENT_LIST_COUNT_NAME(CONCURRENT_LIST)
{
    if (this == NULL) return true;
    *outCount = this->size;
    return false;
}

#define MAKE_CONCURRENT_LIST_SWAP_NAME(x) x ## _swap(x* this, INDEX_TYPE itemIndex1, INDEX_TYPE itemIndex2)
#define GEN_CONCURRENT_LIST_SWAP_NAME(x) MAKE_CONCURRENT_LIST_SWAP_NAME(x)
// bool list_x_swap(list_x* this, INDEX_TYPE itemIndex1, INDEX_TYPE itemIndex2)
bool GEN_CONCURRENT_LIST_SWAP_NAME(CONCURRENT_LIST) {
    if (this == NULL ||
        itemIndex1 < 0 ||
        itemIndex2 < 0 ||
        this->size <= itemIndex1 ||
        this->size <= itemIndex2
    ) return true;
    
    CONCURRENT_LIST_TYPE temp = this->items[itemIndex1];
    this->items[itemIndex1] = this->items[itemIndex2];
    this->items[itemIndex2] = temp;
    return false;
}

#undef MAKE_CONCURRENT_LIST_NEW_NAME
#undef GEN_CONCURRENT_LIST_NEW_NAME
#undef MAKE_CONCURRENT_LIST_DELETE_NAME
#undef GEN_CONCURRENT_LIST_DELETE_NAME
#undef MAKE_CONCURRENT_LIST_NEW_WITHCAPACITY_NAME
#undef GEN_CONCURRENT_LIST_NEW_WITHCAPACITY_NAME
#undef MAKE_CONCURRENT_LIST_INDEXOF_NAME
#undef GEN_CONCURRENT_LIST_INDEXOF_NAME
#undef MAKE_CONCURRENT_LIST_INDEXOF2_NAME
#undef GEN_CONCURRENT_LIST_INDEXOF2_NAME
#undef MAKE_CONCURRENT_LIST_INDEXOF3_NAME
#undef GEN_CONCURRENT_LIST_INDEXOF3_NAME
#undef MAKE_CONCURRENT_LIST_GET_NAME
#undef GEN_CONCURRENT_LIST_GET_NAME
#undef GEN_CONCURRENT_LIST_GET_FIRST_NAME
#undef MAKE_CONCURRENT_LIST_GET_FIRST_NAME
#undef GEN_CONCURRENT_LIST_GET_LAST_NAME
#undef MAKE_CONCURRENT_LIST_GET_LAST_NAME
#undef MAKE_CONCURRENT_LIST_GETRANGE_NAME
#undef GEN_CONCURRENT_LIST_GETRANGE_NAME
#undef MAKE_CONCURRENT_LIST_ADD_NAME
#undef GEN_CONCURRENT_LIST_ADD_NAME
#undef MAKE_CONCURRENT_LIST_ADDRANGE_NAME
#undef GEN_CONCURRENT_LIST_ADDRANGE_NAME
#undef MAKE_CONCURRENT_LIST_ADD_AT_NAME
#undef GEN_CONCURRENT_LIST_ADD_AT_NAME
#undef MAKE_CONCURRENT_LIST_ADDRANGE_AT_NAME
#undef GEN_CONCURRENT_LIST_ADDRANGE_AT_NAME
#undef MAKE_CONCURRENT_LIST_REMOVE_NAME
#undef GEN_CONCURRENT_LIST_REMOVE_NAME
#undef MAKE_CONCURRENT_LIST_REMOVERANGE_NAME
#undef GEN_CONCURRENT_LIST_REMOVERANGE_NAME
#undef MAKE_CONCURRENT_LIST_REMOVE_ITEM_NAME
#undef GEN_CONCURRENT_LIST_REMOVE_ITEM_NAME
#undef MAKE_CONCURRENT_LIST_CLEAR_NAME
#undef GEN_CONCURRENT_LIST_CLEAR_NAME
#undef MAKE_CONCURRENT_LIST_SWAP_NAME
#undef GEN_CONCURRENT_LIST_SWAP_NAME
#undef MAKE_CONCURRENT_LIST_NAME
#undef CONCURRENT_LIST_TYPE_CMP_FUNCTION
#undef MINIMUM_CONCURRENT_LIST_SIZE
#undef CONCURRENT_LIST_NAME
#undef CONCURRENT_LIST
#undef CONCURRENT_LIST_TYPE
