/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifdef __linux__
#include <stdio.h>
#include <stdint.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

_Atomic(int32_t) _debugging_utilities_failed_to_initialize = 0;
_Atomic(int32_t) _debugging_utilities_initialized = 0;
_Atomic(void**) _debugging_utilities_readonly_memory_ranges = NULL;
_Atomic(int32_t) _debugging_utilities_readonly_memory_ranges_count = 0;
static void initialize_debugging_utilities()
{
    int32_t expected = 0, capacity = 16;
    if (atomic_compare_exchange_strong(&_debugging_utilities_initialized, &expected, 1) == false)
        return;
    _debugging_utilities_readonly_memory_ranges = (void**)malloc(sizeof(void*) * capacity);
    if (_debugging_utilities_readonly_memory_ranges == NULL) return;
    FILE* maps = fopen("/proc/self/maps", "r");
    if (maps == NULL) {
        fprintf(stderr, "Could not open /proc/self/maps\n");
        free(_debugging_utilities_readonly_memory_ranges);
        _debugging_utilities_failed_to_initialize = 1;
        return;
    }

    char line[256];

    while (fgets(line, sizeof(line), maps) != NULL) {
        uintptr_t start, end;
        char perms[5];
        if (!sscanf(line, "%lx-%lx %4s", &start, &end, perms))
        {
            fprintf(stderr, "Failed to parse /proc/self/maps for readonly memory section!\n");
            free(_debugging_utilities_readonly_memory_ranges);
            fclose(maps);
            _debugging_utilities_failed_to_initialize = 1;
            return;
        }
        if (perms[0] == 'r' && perms[1] == '-') {
            _debugging_utilities_readonly_memory_ranges_count += 2;
            if (capacity <= _debugging_utilities_readonly_memory_ranges_count)
            {
                capacity *= 2;
                void* newPtr = realloc(_debugging_utilities_readonly_memory_ranges, sizeof(void*) * capacity);
                if (newPtr == NULL)
                {
                    fprintf(stderr, "Failed to reallocate memory for debugging utility.\n");
                    free(_debugging_utilities_readonly_memory_ranges);
                    fclose(maps);
                    _debugging_utilities_failed_to_initialize = 1;
                    return;
                }
                _debugging_utilities_readonly_memory_ranges = newPtr;
            }

           _debugging_utilities_readonly_memory_ranges[_debugging_utilities_readonly_memory_ranges_count - 2] = (void*) start;
           _debugging_utilities_readonly_memory_ranges[_debugging_utilities_readonly_memory_ranges_count - 1] = (void*) end;
        }
    }
    fclose(maps);
    _debugging_utilities_initialized = 2;
}

int debugging_utilities_is_readonly(void* ptr) {
    if (atomic_load(&_debugging_utilities_initialized) == 0)
        initialize_debugging_utilities();
    while (atomic_load(&_debugging_utilities_initialized) < 2 && atomic_load(&_debugging_utilities_failed_to_initialize) != 1)
    {
        struct timespec ts;
        ts.tv_sec = 0;
        ts.tv_nsec = 1000000;
        nanosleep(&ts, NULL);
    }
    if (atomic_load(&_debugging_utilities_failed_to_initialize) == 1)
    {
        fprintf(stderr, "initialize_debugging_utilities() failed to initialize.\n");
        return -1;
    }

    for (int32_t i = 0; i < _debugging_utilities_readonly_memory_ranges_count; i += 2)
    {
        uintptr_t start, end;
        start = (uintptr_t) _debugging_utilities_readonly_memory_ranges[i];
        end = (uintptr_t)  _debugging_utilities_readonly_memory_ranges[i + 1];

        if ((uintptr_t)ptr >= start && (uintptr_t)ptr <= end)
            return 1; // The pointer is found in a read-only section, it is not writeable.
    }
    return 0;  // The pointer is not in a read-only section, or it is writable.
}
#else
#error Debugging Utilities is not supported on other platform than Linux.
#endif
