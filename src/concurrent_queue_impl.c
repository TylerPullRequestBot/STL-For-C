/*
 * Copyright (c) 2023 Tyler Crandall
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CONCURRENT_QUEUE_TYPE
#error There must be a provided type for CONCURRENT_QUEUE_TYPE and it must be defined prior to using this code!
#define CONCURRENT_QUEUE_TYPE int32_t // To make VSCode happy
#endif

#ifndef ALLOW_SHRINKING_QUEUE
#define ALLOW_SHRINKING_QUEUE 1
#endif

#ifndef CONCURRENT_QUEUE_MINIMUM_SIZE
#define CONCURRENT_QUEUE_MINIMUM_SIZE 16
#endif

#ifndef CONCURRENT_QUEUE_MAXIMUM_CAPACITY
#define CONCURRENT_QUEUE_MAXIMUM_CAPACITY 128 // This should be set to the highest available CPU threads on the market.
                                              // By default, this is set to 128 threads limit for future proofing.
                                              // The trade off for this is that it'll require allocation of 128 pointers which
                                              // can adds up to 1 Kilobyte size allocation for each Concurrent Queue by default.
#endif

#if CONCURRENT_QUEUE_MAXIMUM_CAPACITY < CONCURRENT_QUEUE_MINIMUM_SIZE
#define CONCURRENT_QUEUE_MAXIMUM_CAPACITY CONCURRENT_QUEUE_MINIMUM_SIZE
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#ifndef STL_FOR_C_IMPLEMENTATION_ONLY
#define STL_FOR_C_IMPLEMENTATION_ONLY
#endif

#define DEFAULT_EXCHANGE_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_LOAD_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_FETCH_ADD_MEMORY_ORDER memory_order_relaxed
#define DEFAULT_FETCH_SUB_MEMORY_ORDER memory_order_relaxed

#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SYMBOLIZE(x) x
#define QUEUE_TYPE SYMBOLIZE(CONCURRENT_QUEUE_TYPE)
#define PREFIX_NAME internal_for_concurrent_queue_
#define NO_UNDEF 1
#include "src/queue_impl.c"

// CONCURRENT QUEUE SECTION

#define MAKE_CONCURRENT_QUEUE_NAME(x) concurrent_queue_##x
#define CONCURRENT_QUEUE_NAME(x) MAKE_CONCURRENT_QUEUE_NAME(x)
#define CONCURRENT_QUEUE CONCURRENT_QUEUE_NAME(CONCURRENT_QUEUE_TYPE)

#define MAKE_QUEUE_NODE_NAME(x) concurrent_queue_node_##x
#define QUEUE_NODE_NAME(x) MAKE_QUEUE_NODE_NAME(x)
#define QUEUE_NODE QUEUE_NODE_NAME(CONCURRENT_QUEUE_TYPE)

typedef struct QUEUE_NODE {
    volatile QUEUE* queue;
    volatile INDEX_TYPE lastQueueSize;
} QUEUE_NODE;

typedef struct CONCURRENT_QUEUE {
    volatile QUEUE_NODE* queues;
    volatile INDEX_TYPE size;
    volatile INDEX_TYPE failDequeueCount;
} CONCURRENT_QUEUE;

#define MAKE_CONCURRENT_QUEUE_NEW_NAME(x) x##_new()
#define GEN_CONCURRENT_QUEUE_NEW_NAME(x) MAKE_CONCURRENT_QUEUE_NEW_NAME(x)

CONCURRENT_QUEUE*
GEN_CONCURRENT_QUEUE_NEW_NAME(CONCURRENT_QUEUE)
{
#define MAKE_QUEUE_NEW_USE(x) x##_new
#define GEN_QUEUE_NEW_USE(x) MAKE_QUEUE_NEW_USE(x)

    CONCURRENT_QUEUE* newQueue = (CONCURRENT_QUEUE*)malloc(sizeof(CONCURRENT_QUEUE));
    newQueue->queues = (QUEUE_NODE*)calloc(sizeof(QUEUE_NODE), CONCURRENT_QUEUE_MAXIMUM_CAPACITY);
    newQueue->size = CONCURRENT_QUEUE_MINIMUM_SIZE;
    newQueue->failDequeueCount = 0;
#if CONCURRENT_QUEUE_MINIMUM_SIZE > 0
    for (INDEX_TYPE i = 0; i < CONCURRENT_QUEUE_MINIMUM_SIZE; ++i) {
        QUEUE_NODE* node = (QUEUE_NODE*)&newQueue->queues[i];
        node->queue = GEN_QUEUE_NEW_USE(QUEUE)();
        node->lastQueueSize = 0;
    }
#endif
    return newQueue;

#undef GEN_QUEUE_NEW_USE
#undef MAKE_QUEUE_NEW_USE
}

#undef GEN_CONCURRENT_QUEUE_NEW_NAME
#undef MAKE_CONCURRENT_QUEUE_NEW_NAME

#define MAKE_CONCURRENT_QUEUE_DESTROY_NAME(x) x##_destroy(x* this)
#define GEN_CONCURRENT_QUEUE_DESTROY_NAME(x) \
    MAKE_CONCURRENT_QUEUE_DESTROY_NAME(x)

bool GEN_CONCURRENT_QUEUE_DESTROY_NAME(CONCURRENT_QUEUE)
{
#define MAKE_QUEUE_DESTROY_USE(x) x##_destroy
#define GEN_QUEUE_DESTROY_USE(x) MAKE_QUEUE_DESTROY_USE(x)

    if (this == NULL)
        return true;
    
    for (INDEX_TYPE i = 0; i < this->size; ++i)
    {
        QUEUE_NODE* node = (QUEUE_NODE*)&this->queues[i];
        GEN_QUEUE_DESTROY_USE(QUEUE)((QUEUE*)node->queue);
    }
    free((void*)this->queues);
    free(this);
    return false;

#undef GEN_QUEUE_DESTROY_USE
#undef MAKE_QUEUE_DESTROY_USE
}

#undef GEN_CONCURRENT_QUEUE_DESTROY_NAME
#undef MAKE_CONCURRENT_QUEUE_DESTROY_NAME

#define MAKE_CONCURRENT_QUEUE_ENQUEUE_NAME(x, y, z) x##_enqueue(x* this, y value, z* outEnqueuedCount)
#define GEN_CONCURRENT_QUEUE_ENQUEUE_NAME(x, y, z) MAKE_CONCURRENT_QUEUE_ENQUEUE_NAME(x, y, z)

bool GEN_CONCURRENT_QUEUE_ENQUEUE_NAME(CONCURRENT_QUEUE, CONCURRENT_QUEUE_TYPE, INDEX_TYPE)
{
#define MAKE_QUEUE_ENQUEUE_USE(x) x##_enqueue
#define GEN_QUEUE_ENQUEUE_USE(x) MAKE_QUEUE_ENQUEUE_USE(x)
#define MAKE_QUEUE_NEW_USE(x) x##_new
#define GEN_QUEUE_NEW_USE(x) MAKE_QUEUE_NEW_USE(x)
#define MAKE_QUEUE_DESTROY_USE(x) x##_destroy
#define GEN_QUEUE_DESTROY_USE(x) MAKE_QUEUE_DESTROY_USE(x)
    
    if (this == NULL)
        return true;
    
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        QUEUE_NODE* node = (QUEUE_NODE*) &this->queues[i];
        QUEUE* queue = atomic_exchange_explicit((_Atomic(QUEUE*)*)&node->queue, NULL, DEFAULT_EXCHANGE_MEMORY_ORDER);
        
        if (queue == NULL) {
            continue;
        }
        
        INDEX_TYPE actualEnqueuedCount = 0;
        if (GEN_QUEUE_ENQUEUE_USE(QUEUE)((QUEUE*)queue, value, &actualEnqueuedCount)) {
            atomic_store((_Atomic(QUEUE*)*)&node->queue, queue);
            continue;
        }
        atomic_fetch_add((_Atomic(INDEX_TYPE)*)&node->lastQueueSize, actualEnqueuedCount);
        if (outEnqueuedCount != NULL)
            *outEnqueuedCount = actualEnqueuedCount;
        atomic_store((_Atomic(QUEUE*)*)&node->queue, queue);
        return false;
    }
    QUEUE* newQueue = GEN_QUEUE_NEW_USE(QUEUE)();
    if (newQueue == NULL)
        return true;
    
    INDEX_TYPE currentIdx = atomic_fetch_add_explicit((_Atomic(INDEX_TYPE)*)&this->size, 1, DEFAULT_FETCH_ADD_MEMORY_ORDER);
    if (currentIdx >= CONCURRENT_QUEUE_MAXIMUM_CAPACITY)
    {
        atomic_store((_Atomic(INDEX_TYPE)*)&this->size, CONCURRENT_QUEUE_MAXIMUM_CAPACITY); // Let's not cause an integer overflow...
        return true;
    }
    QUEUE_NODE* newNode = (QUEUE_NODE*) &this->queues[currentIdx];
    newNode->queue = newQueue;
    INDEX_TYPE actualEnqueuedCount = 0;
    if (GEN_QUEUE_ENQUEUE_USE(QUEUE)((QUEUE*)newNode->queue, value, &actualEnqueuedCount))
    {
        return true; // We tried...
    }
    atomic_fetch_add((_Atomic(INDEX_TYPE)*)&newNode->lastQueueSize, actualEnqueuedCount);
    if (outEnqueuedCount != NULL)
        *outEnqueuedCount = actualEnqueuedCount;
    return false;

#undef GEN_QUEUE_ENQUEUE_USE
#undef MAKE_QUEUE_ENQUEUE_USE
#undef GEN_QUEUE_NEW_USE
#undef MAKE_QUEUE_NEW_USE
#undef GEN_QUEUE_DESTROY_USE
#undef MAKE_QUEUE_DESTROY_USE
}

#undef GEN_CONCURRENT_QUEUE_ENQUEUE_NAME
#undef MAKE_CONCURRENT_QUEUE_ENQUEUE_NAME

#define MAKE_CONCURRENT_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z) x##_enqueue_items(x *this, y *items, z items_index, z items_count, z *outEnqueuedCount)
#define GEN_CONCURRENT_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z) MAKE_CONCURRENT_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z)

bool GEN_CONCURRENT_QUEUE_ENQUEUE_ITEMS_NAME(CONCURRENT_QUEUE, CONCURRENT_QUEUE_TYPE, INDEX_TYPE)
{
#define MAKE_QUEUE_ENQUEUE_ITEMS_USE(x) x##_enqueue_items
#define GEN_QUEUE_ENQUEUE_ITEMS_USE(x) MAKE_QUEUE_ENQUEUE_ITEMS_USE(x)
#define MAKE_QUEUE_NEW_USE(x) x##_new
#define GEN_QUEUE_NEW_USE(x) MAKE_QUEUE_NEW_USE(x)
#define MAKE_QUEUE_DESTROY_USE(x) x##_destroy
#define GEN_QUEUE_DESTROY_USE(x) MAKE_QUEUE_DESTROY_USE(x)
    
    if (this == NULL)
        return true;
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        QUEUE_NODE* node = (QUEUE_NODE*) &this->queues[i];
        QUEUE* queue = atomic_exchange_explicit((_Atomic(QUEUE*)*)&node->queue, NULL, DEFAULT_EXCHANGE_MEMORY_ORDER);
        
        if (queue == NULL) {
            continue;
        }
        
        INDEX_TYPE actualEnqueuedCount = 0;
        if (GEN_QUEUE_ENQUEUE_ITEMS_USE(QUEUE)((QUEUE*)queue, items, items_index, items_count, &actualEnqueuedCount)) {
            atomic_store((_Atomic(QUEUE*)*)&node->queue, queue);
            continue;
        }
        atomic_fetch_add((_Atomic(INDEX_TYPE)*)&node->lastQueueSize, actualEnqueuedCount);
        if (outEnqueuedCount != NULL)
            *outEnqueuedCount = actualEnqueuedCount;
        atomic_store((_Atomic(QUEUE*)*)&node->queue, queue);
        return false;
    }
    QUEUE* newQueue = GEN_QUEUE_NEW_USE(QUEUE)();
    if (newQueue == NULL)
        return true;
    
    INDEX_TYPE currentIdx = atomic_fetch_add_explicit((_Atomic(INDEX_TYPE)*)&this->size, 1, DEFAULT_FETCH_ADD_MEMORY_ORDER);
    if (currentIdx >= CONCURRENT_QUEUE_MAXIMUM_CAPACITY)
    {
        atomic_store((_Atomic(INDEX_TYPE)*)&this->size, CONCURRENT_QUEUE_MAXIMUM_CAPACITY); // Let's not cause an integer overflow...
        return true;
    }
    QUEUE_NODE* newNode = (QUEUE_NODE*) &this->queues[currentIdx];
    newNode->queue = newQueue;
    INDEX_TYPE actualEnqueuedCount = 0;
    if (GEN_QUEUE_ENQUEUE_ITEMS_USE(QUEUE)((QUEUE*)newNode->queue, items, items_index, items_count, &actualEnqueuedCount))
    {
        return true; // We tried...
    }
    atomic_fetch_add((_Atomic(INDEX_TYPE)*)&newNode->lastQueueSize, actualEnqueuedCount);
    if (outEnqueuedCount != NULL)
        *outEnqueuedCount = actualEnqueuedCount;
    return false;

#undef GEN_QUEUE_ENQUEUE_USE
#undef MAKE_QUEUE_ENQUEUE_USE
#undef GEN_QUEUE_NEW_USE
#undef MAKE_QUEUE_NEW_USE
#undef GEN_QUEUE_DESTROY_USE
#undef MAKE_QUEUE_DESTROY_USE
}

#undef GEN_CONCURRENT_QUEUE_ENQUEUE_NAME
#undef MAKE_CONCURRENT_QUEUE_ENQUEUE_NAME

#define MAKE_CONCURRENT_QUEUE_DEQUEUE_NAME(x, y, z) x##_dequeue(x* this, y* outItem, z* outDequeueCount)
#define GEN_CONCURRENT_QUEUE_DEQUEUE_NAME(x, y, z) MAKE_CONCURRENT_QUEUE_DEQUEUE_NAME(x, y, z)

bool GEN_CONCURRENT_QUEUE_DEQUEUE_NAME(CONCURRENT_QUEUE, CONCURRENT_QUEUE_TYPE, INDEX_TYPE)
{
#define MAKE_QUEUE_DEQUEUE_USE(x) x##_dequeue
#define GEN_QUEUE_DEQUEUE_USE(x) MAKE_QUEUE_DEQUEUE_USE(x)

    if (this == NULL)
        return true;
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        INDEX_TYPE lastCount = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->queues[i].lastQueueSize, DEFAULT_LOAD_MEMORY_ORDER);
        if (lastCount <= 0)
            continue;
        QUEUE_NODE* node = (QUEUE_NODE*) &this->queues[i];
        QUEUE* queue = atomic_exchange_explicit((_Atomic(QUEUE*)*)&node->queue, NULL, DEFAULT_EXCHANGE_MEMORY_ORDER);
        if (queue == NULL) {
            continue;
        }
        INDEX_TYPE actualDequeuedCount = 0;
        if (GEN_QUEUE_DEQUEUE_USE(QUEUE)(queue, outItem, &actualDequeuedCount)) {
            atomic_store((_Atomic(QUEUE*)*)&node->queue, queue);
            continue;
        }
        atomic_fetch_sub((_Atomic(INDEX_TYPE)*)&node->lastQueueSize, 1);
        if (outDequeueCount != NULL)
            *outDequeueCount = actualDequeuedCount;
        atomic_store((_Atomic(QUEUE*)*)&node->queue, queue);
        return false;
    }
    atomic_fetch_add_explicit((_Atomic(INDEX_TYPE)*)&this->failDequeueCount, 1, DEFAULT_FETCH_ADD_MEMORY_ORDER);
    return true;

#undef GEN_QUEUE_DEQUEUE_USE
#undef MAKE_QUEUE_DEQUEUE_USE
}

#undef GEN_CONCURRENT_QUEUE_DEQUEUE_NAME
#undef MAKE_CONCURRENT_QUEUE_DEQUEUE_NAME

#define MAKE_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z) x##_dequeue_items(x *this, z count, y *outItems, z *outDequeueCount)
#define GEN_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z) MAKE_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z)

bool GEN_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME(CONCURRENT_QUEUE, CONCURRENT_QUEUE_TYPE, INDEX_TYPE)
{
#define MAKE_QUEUE_DEQUEUE_ITEMS_USE(x) x##_dequeue_items
#define GEN_QUEUE_DEQUEUE_ITEMS_USE(x) MAKE_QUEUE_DEQUEUE_ITEMS_USE(x)

    if (this == NULL)
        return true;
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        QUEUE_NODE* node = (QUEUE_NODE*) &this->queues[i];
        QUEUE* queue = atomic_exchange_explicit((_Atomic(QUEUE*)*)&node->queue, NULL, DEFAULT_EXCHANGE_MEMORY_ORDER);
        if (queue == NULL) {
            continue;
        }
        INDEX_TYPE actualDequeuedCount = 0;
        if (GEN_QUEUE_DEQUEUE_ITEMS_USE(QUEUE)(queue, count, outItems, &actualDequeuedCount)) {
            atomic_store((_Atomic(QUEUE*)*)&node->queue, queue);
            continue;
        }
        atomic_fetch_sub((_Atomic(INDEX_TYPE)*)&node->lastQueueSize, actualDequeuedCount);
        if (outDequeueCount != NULL)
            *outDequeueCount = actualDequeuedCount;
        atomic_store((_Atomic(QUEUE*)*)&node->queue, queue);
        return false;
    }
    return true;

#undef GEN_QUEUE_DEQUEUE_ITEMS_USE
#undef MAKE_QUEUE_DEQUEUE_ITEMS_USE
}

#undef GEN_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME
#undef MAKE_CONCURRENT_QUEUE_DEQUEUE_ITEMS_NAME

#define MAKE_CONCURRENT_QUEUE_GETCOUNT_NAME(x) x##_getcount(x *this, INDEX_TYPE *outCount)
#define GEN_CONCURRENT_QUEUE_GETCOUNT_NAME(x) MAKE_CONCURRENT_QUEUE_GETCOUNT_NAME(x)

bool GEN_CONCURRENT_QUEUE_GETCOUNT_NAME(CONCURRENT_QUEUE)
{
    if (this == NULL || outCount == NULL)
        return true;
    INDEX_TYPE totalCount = 0;
    INDEX_TYPE size = atomic_load_explicit((_Atomic(INDEX_TYPE)*)&this->size, DEFAULT_LOAD_MEMORY_ORDER);
    for (INDEX_TYPE i = 0; i < size; ++i)
    {
        QUEUE_NODE* node = atomic_load_explicit((_Atomic(QUEUE_NODE*)*) &this->queues[i], DEFAULT_EXCHANGE_MEMORY_ORDER);
        totalCount += atomic_load_explicit((_Atomic(INDEX_TYPE)*)&node->lastQueueSize, DEFAULT_LOAD_MEMORY_ORDER);
    }
    *outCount = totalCount;
    return false;
}

#ifndef NO_UNDEF
#undef GEN_CONCURRENT_QUEUE_GETCOUNT_NAME
#undef MAKE_CONCURRENT_QUEUE_GETCOUNT_NAME

#undef CONCURRENT_QUEUE
#undef CONCURRENT_QUEUE_NAME
#undef MAKE_CONCURRENT_QUEUE_NAME
#undef STL_FOR_C_IMPLEMENTATION_ONLY
#undef INDEX_TYPE
#undef CONCURRENT_QUEUE_TYPE
#undef QUEUE
#undef QUEUE_NAME
#undef MAKE_QUEUE_NAME
#undef MINIMUM_QUEUE_SIZE
#endif
