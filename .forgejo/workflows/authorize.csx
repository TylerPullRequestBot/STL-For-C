#!/usr/bin/env dotnet-script

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Diagnostics;

var APICallURL = "https://codeberg.org";
var APIParameters = $"/api/v1/repos/{Environment.GetEnvironmentVariable("REPOPATH")}/issues/{Environment.GetEnvironmentVariable("PULLREQUESTID")}/comments";

ProcessStartInfo startInfo = new ProcessStartInfo()
{
    FileName = "git",
    Arguments = "rev-parse HEAD",
    RedirectStandardOutput = true,
    UseShellExecute = false,
    CreateNoWindow = true,
};

Process process = new Process() { StartInfo = startInfo };
process.Start();
var currentCommit = process.StandardOutput.ReadToEnd().Trim();
process.WaitForExit();

public class User {
    public long id {get;set;}
    public string login {get;set;}
    public string login_name {get;set;}
    public string full_name {get;set;}
    public string email {get;set;}
    public string avatar_url {get;set;}
    public string language {get;set;}
    public bool is_admin {get;set;}
    public string last_login {get;set;}
    public string created {get;set;}
    public bool restricted {get;set;}
    public bool active {get;set;}
    public bool prohibit_login {get;set;}
    public string location {get;set;}
    public string website {get;set;}
    public string description {get;set;}
    public string visibility {get;set;}
    public long followers_count {get;set;}
    public long following_count {get;set;}
    public long starred_repos_count {get;set;}
    public string username {get;set;}
}

public class Comment {
    public long id {get;set;}
    public string html_url {get;set;}
    public string pull_request_url {get;set;}
    public string issue_url {get;set;}
    public User user {get;set;}
    public string original_author {get;set;}
    public long original_author_id {get;set;}
    public string body {get;set;}
    public object[] assets {get;set;}
    public string created_at {get;set;}
    public string updated_at {get;set;}
}


try {
    var client = new HttpClient();
    client.BaseAddress = new Uri(APICallURL);
    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    var comments = client.GetFromJsonAsync<Comment[]>(APIParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.

    foreach (var c in comments)
    {
        if (c.user.id == 54670 && c.user.login == "TylerLinuxDev" && c.user.username == "TylerLinuxDev" &&
            c.body == $"CI/CD Approved {currentCommit}")
        {
            Environment.Exit(0);
        }
    }

    Environment.Exit(1);
}
catch
{
    Environment.Exit(1);
}
Environment.Exit(1);
