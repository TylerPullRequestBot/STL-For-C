#!/bin/bash

#Sanity checks

echo $USER

if [ -z $USER ]; then
    echo "No secret variables were provided!"
    exit 1
fi
if [ -z $PASS ]; then
    echo "No secret variables were provided!"
    exit 1
fi
if [ -z $TOKEN ]; then
    echo "No secret variables were provided!"
    exit 1
fi
if [ -z $PULLREQUESTID ]; then
    echo "Pull Request ID is not provided!"
    exit 1
fi
if [ -z $REPOPATH ]; then
    echo "Pull Request ID is not provided!"
    exit 1
fi
if [ -z $( which tea ) ]; then
    echo "Tea utility is not installed for pull request reporting!"
    exit 1
fi

tea logins add -n primary --user $USER --token $TOKEN --password $PASS --url https://codeberg.org
tea login default primary
file_content=$( cat "${GITHUB_WORKSPACE}/build/meson-logs/testlog.txt" )
tea c --repo $REPOPATH $PULLREQUESTID $file_content
