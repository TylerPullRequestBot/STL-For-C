# Standard Template Library for C
[![Chat on Matrix](https://img.shields.io/matrix/stl_for_c:matrix.org)](https://matrix.to/#/#stl_for_c:matrix.org) ![License](https://img.shields.io/badge/license-MIT-green)

STL For C is a versatile, free library providing generic data types for the C programming language and beyond.

### Currently Supported Data Types
 - List/Vector (Non-concurrent)
 - Queue (Non-concurrent)
 - Stack (Non-concurrent)
 - Dictionary/Hashtable (Non-concurrent)
 - Event Handler (Non-concurrent)
 - Concurrent Queue
 - Concurrent Stack

### In Development
 - Concurrent List/Vector
 - Concurrent Dictionary/Hashtable
 - Generic Search Features
 - Generic Sorting Features

### Planned Features

 - Vectorization/SIMD Optimization
 - Got an idea? Submit an idea in [discussion](https://codeberg.org/TylerLinuxDev/STL-For-C/issues)!

### On the Horizon
 - Rewrite/Optimize Concurrent Queue and Concurrent Stack for comparable performance as [C++'s concurrent queue](https://github.com/cameron314/concurrentqueue)

### Under Review
 - Hashset
 - Doubly Linked List
    Shelved for now in favor of supporting important data containers first

## Why STL for C?

STL for C has been designed to fulfill certain needs and efficiencies in programming with C. Here are the reasons why it stands out:

 - **Variety of Data Containers**: STL for C offers an extensive range of both thread-safe and non-thread-safe data containers. This provides developers the flexibility to choose the appropriate container based on their specific requirements.

 - **Preprocessor for Type Substitution**: Through leveraging the C preprocessor, STL for C enables data type substitution for each container type. This clever design helps to eliminate the performance overhead usually associated with pointer indirection in traditional C data containers.

 - **Foreign Function Interface (FFI) Considerations**: Our library has been designed with FFI in mind. This means it can be conveniently interfaced and used from various other programming languages, enhancing its versatility and reach.

 - **Efficient Memory Use**: Most of the data containers in STL for C internally use circular buffers. This helps to avoid unnecessary memory reallocations, making the library more efficient.

 - **Filling the Gap**: STL for C fills a crucial niche in the C programming community. It addresses the question, "Why hasn't something like this been created before?" by providing a solution that complements and enhances the capabilities of the C language.

By choosing STL for C, you're opting for a versatile, efficient, and accessible library tailored to meet your needs in the C programming environment.

## How to use this library?

For instructions on how to use this library, please [**visit the docs**](https://codeberg.org/TylerLinuxDev/STL-For-C/wiki)

## Licensing

This project is licensed under the terms of the MIT license. For more details, please refer to the [LICENSE](LICENSE) document.

## Disclaimer of Warranty

As you incorporate this software into your projects, we encourage you to remember that it comes with a Disclaimer of Warranty.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

We sincerely appreciate your interest in our software. However, we advise that you use it responsibly and understand that you do so at your own risk. Your understanding and cooperation are much appreciated!
